from django.shortcuts import render
from django.views.generic.base import View


from collections import OrderedDict

# Create your views here.




########################################################################
class Home(View):
    """"""

    template = "home.html"

    #----------------------------------------------------------------------
    def get(self, request):
        """"""

        pythonskills = [
            ("https://www.djangoproject.com/", "Django"),
            ("http://www.numpy.org/", "Numpy"),
            ("http://opencv.org/", "OpenCV"),
            ("http://qt-project.org/wiki/PySide", "PySide"),
            ("http://www.riverbankcomputing.co.uk/software/pyqt/intro", "PyQt"),
            ("http://www.wxpython.org/", "WxPython"),
            ("https://wiki.python.org/moin/TkInter", "TkInter"),
            ("http://matplotlib.org/", "Matplotlib"),
            ("http://www.scipy.org/", "SciPy"),
            ("http://brython.info/", "Brython"),
            ("http://pygame.org/", "PyGame"),
            ("http://pygame.org/", "Kivy"),
        ]
        pythonskills.sort(key=lambda x: x[1])


        electronicskills = [
            ("", "Prototyping"),
            ("", "Embedded systems"),
            ("", "Pinguino"),
            ("", "Raspberry Pi"),
            ("", "Interfaces Hardware-Software"),
            ("", "Data acquisition"),
        ]
        electronicskills.sort(key=lambda x: x[1])


        i_develop = [
            ("", "Neural networks"),
            ("", "Artificial vision"),
            ("", "Real time systems"),
            ("", "Scientific computing"),
        ]
        i_develop.sort(key=lambda x: x[1])



        interests = [
            ("", "Internet of things"),
            ("", "Domotic"),
            ("", "Tomatos"),
            ("", "Electronic art"),
            ("", "Augmented reality"),
        ]
        interests.sort(key=lambda x: x[1])


        web = [
            ("http://en.wikipedia.org/wiki/HTML5", "HTML5"),
            ("http://en.wikipedia.org/wiki/JavaScript", "JavaScript"),
            ("http://jquery.com/", "JQuery"),
            ("http://en.wikipedia.org/wiki/Cascading_Style_Sheets", "CSS3"),
            ("http://coffeescript.org/", "CoffeeScript"),
            ("http://brython.info/", "Brython"),
        ]
        web.sort(key=lambda x: x[1])


        frameworks = [
            ("http://getbootstrap.com/", "Bootstrap"),
            ("http://django-rest-framework.org/", "Django REST framework"),
            ("https://www.djangoproject.com/", "Django"),
            ("http://fortawesome.github.io/Font-Awesome", "Font Awesome"),
        ]
        frameworks.sort(key=lambda x: x[1])


        ides = [
            ("http://wingware.com/", "Wing IDE Professional"),
            ("http://pinguino.cc/", "Pinguino IDE"),
        ]
        ides.sort(key=lambda x: x[1])


        domain = [
            ("https://bitcoin.org/", "Bitcoin"),
            ("http://www.ubuntu.com/", "Ubuntu"),
            ("https://www.archlinux.org/", "Arch Linux"),
            ("http://archlinuxarm.org/", "Arch Linux ARM (Raspberry Pi)"),
        ]
        domain.sort(key=lambda x: x[1])

        learning = [
            ("http://nodejs.org/", "Node.Js"),
            ("http://www.postgresql.org/", "PostgreSQL"),
            ("http://www.tornadoweb.org/", "Tornado"),
            ("http://www.ros.org/", "ROS"),
            ("https://www.docker.com/", "Docker"),
            ("http://www.openscad.org/", "OpenSCAD"),
        ]
        learning.sort(key=lambda x: x[1])


        android = [
            ("http://kivy.org/", "Kivy"),
            ("https://github.com/kivy/python-for-android", "Python for Android"),
            ("http://www.getmdl.io/", "Material Design Lite"),
        ]
        android.sort(key=lambda x: x[1])


        skills = OrderedDict()

        skills["Python"] = pythonskills
        skills["Electronic"] = electronicskills
        skills["I develop"] = i_develop
        skills["Web development"] = web
        skills["Android development"] = android
        skills["Frameworks"] = frameworks
        skills["IDEs"] = ides
        skills["Works"] = domain
        skills["Learning"] = learning
        skills["Interest"] = interests




        return render(request, self.template, locals())






